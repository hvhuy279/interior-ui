import "./App.css";
import Banner from "./components/Banner/Banner";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Hero from "./components/Hero/Hero";
import Interior from "./components/Interior/Interior";
import Product from "./components/Product/Product";
import Service from "./components/Service/Service";

function App() {
  return (
    <div>
      <div className="background-hero">
        <Header />
        <Hero />
      </div>
      <Service />
      <Product />
      <Interior />
      <Banner />
      <Footer />
    </div>
  );
}

export default App;
