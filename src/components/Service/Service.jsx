import React, { Component } from "react";
import shippingIcon from "../../assets/icons/shipping-box.svg";
import walletIcon from "../../assets/icons/wallet.svg";
import customerIcon from "../../assets/icons/customer-service.svg";
import "./Service.css";

class Service extends Component {
  render() {
    return (
      <section className="service">
        <div className="service-container">
          <div className="service__item">
            <img
              src={shippingIcon}
              alt="shipping-icon"
              className="service__item--icon"
            />
            <div className="service__item--info">
              <h4>Free Shapping</h4>
              <p>No charge for each delivery</p>
            </div>
          </div>
          <div className="service__item">
            <img
              src={walletIcon}
              alt="wallet-icon"
              className="service__item--icon"
            />
            <div className="service__item--info">
              <h4>Quick Payment</h4>
              <p>100% secure payment</p>
            </div>
          </div>
          <div className="service__item">
            <img
              src={customerIcon}
              alt="customer-icon"
              className="service__item--icon"
            />
            <div className="service__item--info">
              <h4>24/7 Support</h4>
              <p>Quick support</p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Service;
