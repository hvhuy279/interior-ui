import React, { Component } from "react";
import "./Banner.css";

class Banner extends Component {
  render() {
    return (
      <section className="banner">
        <h2 className="banner__title">
          Ready for a <span className="banner__title--visit">Site visit ?</span>
        </h2>
        <p className="banner__desc">Lorem ipsum dolo elit Lorem ipsum dolo</p>
        <button className="banner__btn">View Now</button>
      </section>
    );
  }
}

export default Banner;
