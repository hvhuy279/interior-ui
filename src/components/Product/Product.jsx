import React, { Component } from "react";
import arrowRight from "../../assets/icons/arrow-right.svg";
import "./Product.css";
import ProductItem from "./ProductItem/ProductItem";
import productItem1 from "../../assets/images/product-1.jpg";
import productItem2 from "../../assets/images/product-2.jpg";
import productItem3 from "../../assets/images/product-3.jpg";

class Product extends Component {
  render() {
    return (
      <section className="product">
        <div className="product-container">
          <h2 className="product__title">
            Best Seller
            <br />
            Product
          </h2>
          <p className="product__more">
            See all colection
            <img src={arrowRight} alt="arrow-right-icon" />
          </p>
        </div>

        <div className="product-list">
          <ProductItem imageProduct={productItem1} />
          <ProductItem imageProduct={productItem2} />
          <ProductItem imageProduct={productItem3} />
          <ProductItem imageProduct={productItem3} />
        </div>
      </section>
    );
  }
}

export default Product;
