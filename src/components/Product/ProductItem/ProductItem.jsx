import React, { Component } from "react";
import "./ProductItem.css";
import cartIcon from "../../../assets/icons/shopping-cart.svg";

class ProductItem extends Component {
  render() {
    return (
      <div className="product__item">
        <div className="product__item--image">
          <img src={this.props.imageProduct} alt="product-img" />
        </div>
        <div className="product__item--name">
          <p>Modern Picture</p>
          <img src={cartIcon} alt="cart-icon" />
        </div>
        <div className="product__item--price">$15.00</div>
        <div className="product__item--label">
          <div className="label">Label</div>
          <div className="label">Label</div>
          <div className="label">Label</div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
