import React, { Component } from "react";
import cartShoppingIcon from "../../assets/icons/cart.svg";
import "./Header.css";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="header-container">
          <ul className="header__menu">
            <li className="header__menu--item">
              <a href="/">Home</a>
            </li>
            <li className="header__menu--item">
              <a href="/shop">Shop</a>
            </li>
            <li className="header__menu--item">
              <a href="/about">About Us</a>
            </li>
            <li className="header__menu--item">
              <a href="/contact">Contact</a>
            </li>
          </ul>
          <div className="header__shop">
            <img
              src={cartShoppingIcon}
              alt="shopping-cart"
              className="header__shop--image"
            />
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
