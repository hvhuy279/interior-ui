import React, { Component } from "react";
import "./Interior.css";
import livingRoom1 from "../../assets/images/living-room-1.jpg";
import livingRoom2 from "../../assets/images/living-room-2.jpg";
import livingRoom3 from "../../assets/images/living-room-3.jpg";
import livingRoom4 from "../../assets/images/living-room-4.jpg";

class Interior extends Component {
  render() {
    return (
      <section className="interior">
        <div className="interior-container">
          <h2 className="interior__title">Interior Plant Reference</h2>
          <p className="interior__desc">
            make your home so comfortable with refreshing plants
          </p>

          <div className="interior__product">
            <div className="interior__living">
              <img
                src={livingRoom1}
                alt="living-room"
                className="living-room-1"
              />
              <img
                src={livingRoom2}
                alt="living-room"
                className="living-room-2"
              />
            </div>
            <div className="interior__all">
              <img
                src={livingRoom3}
                alt="living-room"
                className="living-room-3"
              />
              <img
                src={livingRoom4}
                alt="living-room"
                className="living-room-4"
              />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Interior;
