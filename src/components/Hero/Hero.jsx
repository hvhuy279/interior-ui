import React, { Component } from "react";
import "./Hero.css";
import searchIcon from "../../assets/icons/search.svg";
import fragnasBox from "../../assets/images/fragnas.png";
import fragnasPoint from "../../assets/images/point-fragnas.svg";
import starIcon from "../../assets/icons/star.svg";
import userAvatar from "../../assets/images/user.png";

class Hero extends Component {
  render() {
    return (
      <section className="hero">
        <div className="hero-container">
          <h2 className="hero__title">
            Bring Serenity to Your Place
            <br />
            With Interior
          </h2>
          <p className="hero__desc">
            find your dream plant for you home decoration
            <br />
            with us, and we will make it happen.
          </p>
          <div className="hero__search">
            <input
              type="text"
              placeholder="Search plant"
              className="hero__search--input"
            />
            <img
              src={searchIcon}
              alt="search-icon"
              className="hero__search--btn"
            />
          </div>

          <img src={fragnasBox} alt="fragnas-box" className="fragnas-box" />
          <img
            src={fragnasPoint}
            alt="fragnas-point"
            className="fragnas-point"
          />
          <img src={userAvatar} alt="user-avatar" className="user-avatar" />

          <div className="rating__box">
            <h4 className="rating__box--name">Milan Jack</h4>
            <div className="rating__box--info">
              <p>Home Seller, USA</p>
              <div className="rating__vote">
                <img
                  src={starIcon}
                  alt="star-icon"
                  style={{ display: "block" }}
                />
                <p>4.9 Rating</p>
              </div>
            </div>
            <p className="rating__box--param">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro
              beatae error laborum.
            </p>
          </div>

          <div className="hero__shadow"></div>
        </div>
      </section>
    );
  }
}

export default Hero;
