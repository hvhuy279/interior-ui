import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="footer-container">
          <div className="footer__input">
            <input
              type="text"
              placeholder="Enter your email"
              className="footer__input--email"
            />
            <button className="footer__input--btn">Subcribe</button>
          </div>

          <div className="footer__social">
            <ul className="footer__link">
              <h5 className="footer__link--title">Support</h5>
              <li className="footer__link--item">
                <a href="/about">About Us</a>
              </li>
              <li className="footer__link--item">
                <a href="/career">Careers</a>
              </li>
              <li className="footer__link--item">
                <a href="/blog">Blog</a>
              </li>
            </ul>
            <ul className="footer__link">
              <h5 className="footer__link--title">Useful Link</h5>
              <li className="footer__link--item">
                <a href="/payment">Payment & Tax</a>
              </li>
              <li className="footer__link--item">
                <a href="/team">Team of service</a>
              </li>
              <li className="footer__link--item">
                <a href="/privacy">Privaci Policy</a>
              </li>
            </ul>
            <ul className="footer__link">
              <h5 className="footer__link--title">Our Menu</h5>
              <li className="footer__link--item">
                <a href="/product">Best Product</a>
              </li>
              <li className="footer__link--item">
                <a href="/category">Category</a>
              </li>
            </ul>
            <ul className="footer__link">
              <h5 className="footer__link--title">Address</h5>
              <li className="footer__link--item">
                <a href="/address">
                  JL. Setiabudhi No. 193 Sukasari, Bandung
                  <br />
                  West Java, Indonesia
                </a>
              </li>
              <li className="footer__link--item">
                <a href="mailto:">hallo@daunku.com</a>
              </li>
            </ul>
          </div>
        </div>

        <hr style={{ margin: "3.25rem auto", width: "95rem" }} />

        <p className="footer__copyright">
          © 2023 davixq - All rights reserved.
        </p>
      </footer>
    );
  }
}

export default Footer;
